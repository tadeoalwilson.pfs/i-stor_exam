<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user() == null) {
            $user_id = auth()->user()->id;
            if (auth()->user()->role == 'seller') {
                $products = Product::where('user_id', $user_id)->get();
                $product_ids = [];
                foreach ($products as $product) {
                    $product_ids[] = $product->id;
                }
                $orders = Order::whereIn('product_id', $product_ids)->paginate(8);
                return view('orders.index')->with('orders', $orders);
            }
            $orders = Order::where('user_id', $user_id)->paginate(8);
            return view('orders.index')->with('orders', $orders);
        }
        return redirect()->route('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id',
            'count' => 'required'
        ]);

        $product = Product::where('id', $request->product_id)->first();
        if ($product->stock >= 1 && $request->count <= $product->stock) {
            $product->stock = $product->stock - $request->count;
            $product->save();

            $order = new Order();
            $order->user_id = auth()->user()->id;
            $order->product_id = $request->product_id;
            $order->count = $request->count;
            $order->total_price = ($request->count * $product->price);
            $order->status = 'To Ship';
            $order->save();
            return redirect()->route('products.index')
                ->with('success', 'You have ordered ' . $order->count .
                    ' piece/s of ' . $product->name .
                    ' for a price of ' . $product->price .
                    ' each the Total Price is ' . $order->total_price);
        }
        return redirect()->route('products.index')
            ->with('error', 'This product is out of Stock or You exceed the allowed number of items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $order = Order::find($id);
        $order->status = $request->status;
        $order->save();

        return redirect()->route('orders.index')
            ->with('success', 'Item ' . $order->product->name . ' has been ' . $order->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
