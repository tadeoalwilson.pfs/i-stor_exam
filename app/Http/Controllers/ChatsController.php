<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Illuminate\Http\Request;

class ChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user() == null) {
            if (auth()->user()->role == 'user') {
                $user_id = auth()->user()->id;
                $inboxes = Chat::select('to_user_id')->where('user_id', $user_id)->distinct('to_user_id')->get();
                $to_user_ids = [];
                foreach ($inboxes as $inbox) {
                    $to_user_ids[] = $inbox->to_user_id;
                }
                $contacts = User::whereIn('id', $to_user_ids)->paginate(8);
                return view('chats.index')
                    ->with('contacts', $contacts);
            } elseif (auth()->user()->role == 'seller') {
                $user_id = auth()->user()->id;
                $inboxes = Chat::select('user_id')->where('to_user_id', $user_id)->distinct('user_id')->get();
                $user_ids = [];
                foreach ($inboxes as $inbox) {
                    $user_ids[] = $inbox->user_id;
                }
                $contacts = User::whereIn('id', $user_ids)->paginate(8);
                return view('chats.index')
                    ->with('contacts', $contacts);
            }
        }
        return redirect()->route('products.index');
    }

    public function message($to_user_id)
    {
        if (!auth()->user() == null) {
            $user_id = auth()->user()->id;
            $seller = User::where('id', $to_user_id)->first();
            $messages = Chat::where('to_user_id', $to_user_id)
                ->where('user_id', $user_id)
                ->orWhere('to_user_id', $user_id)
                ->where('user_id', $to_user_id)
                ->orderBy('created_at', 'desc')->take(5)->get();
            return view('chats.message')
                ->with('messages', $messages)
                ->with('seller', $seller);
        }
        return redirect()->route('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user() == null) {
            return view('chats.create');
        }
        return redirect()->route('products.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required',
            'message' => 'required'
        ]);

        if (!auth()->user() == null) {
            if (auth()->user()->role == 'seller') {
                $user = User::where('name', $request->user)->where('role', 'user')->first();
                $chat = new Chat();
                $chat->user_id = auth()->user()->id;
                $chat->to_user_id = $user->id;
                $chat->message = $request->message;
                $chat->save();

                return redirect()->route('chats.message', $user->id);
            } elseif (auth()->user()->role == 'user') {
                $user = User::where('name', $request->user)->where('role', 'seller')->first();
                $chat = new Chat();
                $chat->user_id = auth()->user()->id;
                $chat->to_user_id = $user->id;
                $chat->message = $request->message;
                $chat->save();

                return redirect()->route('chats.message', $user->id);
            }
        } else {
            return redirect()->route('chats.create')
                ->with('error', 'No Seller with such name exist');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
