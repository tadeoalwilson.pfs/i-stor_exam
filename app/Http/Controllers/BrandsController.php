<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\User;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user() == null && auth()->user()->role == 'seller') {
            $user_id = auth()->user()->id;
            $brands = Brand::where('user_id', $user_id)->paginate(8);
            return view('brands.index')->with('brands', $brands);
        }
        return redirect()->route('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user() == null && auth()->user()->role == 'seller') {
            return view('brands.create');
        }
        return redirect()->route('products.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $brand = new Brand();
        $brand->user_id = auth()->user()->id;
        $brand->name = $request->name;
        $brand->save();

        return redirect()->route('brands.index')
            ->with('success', 'Brand ' . $brand->name . ' Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        if (!auth()->user() == null && (auth()->user()->id == $brand->user->id && auth()->user()->role == 'seller')) {
            return view('brands.edit')->with('brand', $brand);
        }
        return redirect()->route('brands.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $brand = Brand::find($id);
        $brand->name = $request->name;
        $brand->save();

        return redirect()->route('brands.index')
            ->with('success', 'Brand ' . $brand->name . ' has been Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();

        return redirect()->route('brands.index')
            ->with('error', 'Brand ' . $brand->name . ' has beend Removed');
    }
}
