<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Brand;
use App\Category;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $sort)
    {
        $products = Product::orderBy('price', 'asc')->paginate(9);
        $brands = Brand::orderBy('name', 'asc')->get();
        $brand = ['name' => ''];
        if (!auth()->user() == null && auth()->user()->role == 'seller') {
            $user_id = auth()->user()->id;
            $brands = Brand::where('user_id', $user_id)->orderBy('name', 'asc')->get();
            $products = Product::where('user_id', $user_id)->orderBy('price', 'desc')->paginate(9);
            if (!$sort->price_from == null && !$sort->price_to == null && !$sort->sort_brand == null) {
                $brand = Brand::select('id', 'name')->where('id', $sort->sort_brand)->first();
                $products = Product::where('user_id', $user_id)
                    ->where('price', '>=', $sort->price_from)
                    ->where('price', '<=', $sort->price_to)
                    ->where('brand_id', $sort->sort_brand)
                    ->orderBy('price', 'desc')->paginate(9);
            } elseif (!$sort->price_from == null && !$sort->price_to == null) {
                $products = Product::where('user_id', $user_id)
                    ->where('price', '>=', $sort->price_from)
                    ->where('price', '<=', $sort->price_to)
                    ->orderBy('price', 'desc')->paginate(9);
            } elseif (!$sort->sort_brand == null) {
                $brand = Brand::select('id', 'name')->where('id', $sort->sort_brand)->first();
                $products = Product::where('user_id', $user_id)
                    ->where('brand_id', $sort->sort_brand)
                    ->orderBy('price', 'desc')->paginate(9);
            }
        } elseif (!$sort->price_from == null && !$sort->price_to == null && !$sort->sort_brand == null) {
            $brand = Brand::select('id', 'name')->where('id', $sort->sort_brand)->first();
            $products = Product::where('price', '>=', $sort->price_from)
                ->where('price', '<=', $sort->price_to)
                ->where('brand_id', $sort->sort_brand)
                ->orderBy('price', 'desc')->paginate(9);
        } elseif (!$sort->price_from == null && !$sort->price_to == null) {
            $products = Product::where('price', '>=', $sort->price_from)
                ->where('price', '<=', $sort->price_to)
                ->orderBy('price', 'desc')->paginate(9);
        } elseif (!$sort->sort_brand == null) {
            $brand = Brand::select('id', 'name')->where('id', $sort->sort_brand)->first();
            $products = Product::where('brand_id', $sort->sort_brand)
                ->orderBy('price', 'desc')->paginate(9);
        }
        return view('products.index')
            ->with('products', $products)
            ->with('price_from', $sort->price_from)
            ->with('price_to', $sort->price_to)
            ->with('brand', $brand)
            ->with('brands', $brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user() == null && auth()->user()->role == 'seller') {
            $user_id = auth()->user()->id;
            $brands = Brand::where('user_id', $user_id)
                ->orderBy('name', 'asc')->get();
            $categories = Category::where('user_id', $user_id)
                ->orderBy('name', 'asc')->get();
            return view('products.create')
                ->with('brands', $brands)
                ->with('categories', $categories);
        }
        return redirect()->route('products.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'category' => 'required',
            'brand' => 'required'
        ]);

        $product = new Product;
        $product->user_id = auth()->user()->id;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->save();
        return redirect()->route('products.index')
            ->with('success', 'Product ' . $product->name . ' Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        if (!auth()->user() == null && (auth()->user()->id == $product->user->id && auth()->user()->role == 'seller')) {
            $user_id = auth()->user()->id;
            $brands = Brand::where('user_id', $user_id)
                ->orderBy('name', 'asc')->get();
            $categories = Category::where('user_id', $user_id)
                ->orderBy('name', 'asc')->get();
            return view('products.edit')
                ->with('product', $product)
                ->with('brands', $brands)
                ->with('categories', $categories);
        }
        return redirect()->route('products.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'category' => 'required',
            'brand' => 'required'
        ]);

        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->save();

        return redirect()->route('products.show', $id)
            ->with('success', 'Product ' . $product->name . ' Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect()->route('products.index')
            ->with('error', 'Product ' . $product->name . ' has been removed');
    }
}
