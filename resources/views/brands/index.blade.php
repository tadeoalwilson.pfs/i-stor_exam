@extends('layouts.app')

@section('content')
    <div class="card mx-auto" style="width: 35rem">
        <div class="card-header">
            Brand List
        </div>
        <div class="card-body">
            @unless(auth::guest())
                <a href="{{ route('brands.create') }}" class="btn btn-primary"><b>+</b> Add Brand</a><br><br>
            @endunless
            @if (count($brands) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($brands as $brand)
                            <tr>
                                <td>{{ $brand->name }}</td>
                                <form action="{{ route('brands.destroy', $brand->id) }}" method="POST">
                                    @csrf
                                    {{-- <input type="hidden" name="_method" value="DELETE"> --}}
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('brands.edit', $brand->id) }}">Update</a>
                                        {{-- <input type="submit" value="Delete" class="btn btn-danger"> --}}
                                    </td>
                                </form>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Brand is Empty</p>
            @endif
            <div>{{ $brands->links() }}</div>
        </div>
    </div>
@endsection
