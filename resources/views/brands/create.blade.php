@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('brands.store') }}">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Add Brand
            </div>
            @include('brands.form')
        </div>
    </form>
@endsection
