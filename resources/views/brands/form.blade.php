@csrf
<div class="card-body">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $brand->name ?? '' }}"
            placeholder="Brand Name">
    </div>
    <a class="btn btn-primary" href="{{ route('brands.index') }}">Back</a>
    <input type="submit" value="Save" class="btn btn-success"
        onclick="this.disabled=true;this.value='Processing...';this.form.submit();"  >
</div>
