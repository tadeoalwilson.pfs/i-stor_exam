@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('brands.update', $brand->id) }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Update Brand
            </div>
            @include('brands.form')
        </div>
    </form>
@endsection
