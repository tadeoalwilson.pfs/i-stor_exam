@extends('layouts.app')

@section('content')
    <div class="card mx-auto" style="width: 35rem">
        <div class="card-header">
            Category List
        </div>
        <div class="card-body">
            <a href="{{ route('categories.create') }}" class="btn btn-primary"><b>+</b> Add Category</a><br><br>
            @if (count($categories) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                    @csrf
                                    {{-- <input type="hidden" name="_method" value="DELETE">
                                    --}}
                                    <td>
                                        <a class="btn btn-primary"
                                            href="{{ route('categories.edit', $category->id) }}">Update</a>
                                        {{-- <input type="submit" value="Delete"
                                            class="btn btn-danger"> --}}
                                    </td>
                                </form>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Category is Empty</p>
            @endif
            <div>{{ $categories->links() }}</div>
        </div>
    </div>
@endsection
