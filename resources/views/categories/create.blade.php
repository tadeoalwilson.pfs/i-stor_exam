@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('categories.store') }}">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Add Category
            </div>
            @include('categories.form')
        </div>
    </form>
@endsection
