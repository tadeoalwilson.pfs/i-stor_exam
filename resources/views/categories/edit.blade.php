@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('categories.update', $category->id) }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Update Category
            </div>
            @include('categories.form')
        </div>
    </form>
@endsection
