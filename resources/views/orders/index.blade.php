@extends('layouts.app')

@section('content')
    <div class="card mx-auto">
        <div class="card-header">
            @if (auth::user()->role == 'user')
                My Order/s
            @else
                Order/s to Ship
            @endif
        </div>
        <div class="card-body">
            @if (count($orders) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Product Name</th>
                            <th scope="col">Count</th>
                            <th scope="col">Price</th>
                            <th scope="col">Total Price</th>
                            <th scope="col">Status</th>
                            <th scope="col">Date Of Order</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td><a
                                        href="{{ route('products.show', $order->product->id) }}">{{ $order->product->name }}</a>
                                </td>
                                <td>{{ $order->count }}</td>
                                <td>{{ $order->product->price }}</td>
                                <td>{{ $order->total_price }}</td>
                                <td>{{ $order->status }}</td>
                                <td>{{ $order->created_at->toDateString() }}</td>
                                @if (auth::user()->role == 'user')
                                    @if ($order->status == 'Shipped')
                                        <form method="POST" action="{{ route('orders.update', $order->id) }}">
                                            <input type="hidden" name="_method" value="PUT">
                                            @csrf
                                            <input type="hidden" name="status" id="status" value="Received">
                                            <td><input type="submit" value="Receive" class="btn btn-success"
                                                    onclick="this.disabled=true;this.value='Processing...';this.form.submit();">
                                            </td>
                                        </form>
                                    @endif
                                @else
                                    @if ($order->status == 'To Ship')
                                        <form method="POST" action="{{ route('orders.update', $order->id) }}">
                                            <input type="hidden" name="_method" value="PUT">
                                            @csrf
                                            <input type="hidden" name="status" id="status" value="Shipped">
                                            <td><input type="submit" value="Ship" class="btn btn-success"
                                                    onclick="this.disabled=true;this.value='Processing...';this.form.submit();">
                                            </td>
                                        </form>
                                    @endif
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                @if (auth::user()->role == 'user')
                    You have no Order/s
                @else
                    You have no Item/s to Ship
                @endif
            @endif
            <div>{{ $orders->links() }}</div>
        </div>
    </div>
@endsection
