@extends('layouts.app')

@section('content')
    <div class="card mx-auto" style="width: 30rem">
        <div class="card-header">
            {{ $product->name }}
        </div>
        <div class="card-body">
            <p>{{ $product->description }}</p>
            <small>Price: ₱{{ $product->price }}</small> |
            <small>Stock: {{ $product->stock }}</small> <br>
            <small>Category:{{ $product->category->name }}</small> <br>
            <small>Brand:{{ $product->brand->name }}</small> <br>
            <small>Seller:{{ $product->user->name }}</small> <br>
            <div class="mx-auto row">
                <a class="btn btn-primary" href="{{ route('products.index') }}">Back</a>
                <div class="col-md-10">
                    @unless(auth::guest())
                        @if (auth()->user()->id == $product->user->id)
                            <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <div class="float-right">
                                    <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">Update</a>
                                    <input class="btn btn-danger" type="submit" value="Delete">
                                </div>
                            </form>
                        @endif
                    @endunless
                    @unless(auth::guest())
                        @if (auth()->user()->role == 'user')
                            <form method="POST" action="{{ route('orders.store') }}">
                                @csrf
                                <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
                                <div class="float-right">
                                    <input type="submit" value="Order Now" class="btn btn-success"
                                        onclick="this.disabled=true;this.value='Processing...';this.form.submit();">
                                </div>
                                <div class="float-right">
                                    <input style="width: 5em" type="number" class="form-control" id="count" name="count" min="0"
                                        placeholder="0">
                                </div>
                                <label class="float-right" for="count">Count:</label>
                            </form>
                        @endif
                    @endunless
                </div>
            </div>
        </div>
    </div>
@endsection
