@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('products.store') }}">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Add Product
            </div>
            @include('products.form')
        </div>
    </form>
@endsection
