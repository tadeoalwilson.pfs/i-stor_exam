@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('products.update', $product->id) }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Update Product
            </div>
            @include('products.form')
        </div>
    </form>
@endsection
