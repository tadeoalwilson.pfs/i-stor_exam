@csrf
<div class="card-body">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $product->name ?? '' }}"
            placeholder="Product Name">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" name="description"
            placeholder="Product Description">{{ $product->description ?? '' }}</textarea>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="price">Price</label>
                <input type="number" class="form-control" id="price" name="price" min="1"
                    value="{{ $product->price ?? '' }}" placeholder="₱ 0.00">
            </div>
        </div>
        <div class="form-group">
            <label for="stock">Stock</label>
            <input type="number" class="form-control" id="stock" name="stock" min="0"
                value="{{ $product->stock ?? '' }}" placeholder="0">
        </div>
        <div class="form-group col-sm-6">
            <label for="category">Category</label>
            <select name="category" id="category" class="form-control">
                <option value=>Select One</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="brand">Brand</label>
            <select name="brand" id="brand" style="width: 12rem" class="form-control">
                <option value=>Select One</option>
                @foreach ($brands as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <a class="btn btn-primary" href="{{ route('products.index') }}">Back</a>
    <input type="submit" value="Save" class="btn btn-success"
        onclick="this.disabled=true;this.value='Processing...';this.form.submit();">
</div>
