@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>List's of Product</h1>
        @unless(auth::guest())
            @if (auth::user()->role == 'seller')
                <a href="{{ route('products.create') }}" class="btn btn-primary"><b>+</b> Add Product</a> <br><br>
            @endif
        @endunless
        <div class="container">
            <form action="{{ route('products.index') }}" method="GET">
                <div class="form-group row">
                    <div class="col-md-1">
                        <label>Filter by Price:</label>
                    </div>
                    <label for="price_from">From:</label>
                    <div class="col-md-2">
                        <input type="number" class="form-control" name="price_from" id="price_from" min="1"
                            placeholder="₱ 0.00" value="{{ $price_from ?? '' }}">
                    </div><br><br>
                    <label for="price_to">To:</label>
                    <div class="col-md-2">
                        <input type="number" class="form-control" name="price_to" id="price_to" min="1" placeholder="₱ 0.00"
                            value="{{ $price_to ?? '' }}">
                    </div><br><br>
                    <label for="sort_brand">Filter by:</label>
                    <div class="col-md-2">
                        <select name="sort_brand" id="sort_brand" class="form-control">
                            @if (!$brand['name'] == '')
                                <option value="{{ $brand['id'] }}">--{{ $brand['name'] }}--</option>
                                <option value="">-----</option>
                            @else
                                <option value="">-----</option>
                            @endif
                            @foreach ($brands as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input type="submit" class="btn btn-primary" value="Search">
                    </div>
                </div>
            </form>
        </div>
        @if (count($products) > 0)
            <div class="row">
                @foreach ($products as $product)
                    <div class="col-sm-4"> &nbsp;
                        <div class="card mx-auto">
                            <img class="card-img-top" src="D:\Al Wilson\Trabaho\I-Stor\gallery_icon.png"
                                alt="Card image cap">
                            <div class="card-body">
                                <h3 class="card-title">{{ $product->name }}</h3>
                                <p class="card-text">{{ $product->description }}</p>
                                <small>Price: ₱{{ $product->price }}</small> |
                                <small>Stock:{{ $product->stock }}</small> <br>
                                <small>Category:{{ $product->category->name }}</small> <br>
                                <small>Brand:{{ $product->brand->name }}</small> <br>
                                <small>Selller:{{ $product->user->name }}</small>
                                <a href="{{ route('products.show', $product->id) }}"
                                    class="btn btn-primary float-right">View</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> <br>
            {{ $products->links() }}
        @else
            <p> Product is Empty </p>
        @endif
    </div>
@endsection
