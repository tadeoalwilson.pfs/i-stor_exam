@extends('layouts.app')

@section('content')
    <div class="card mx-auto" style="width: 35rem">
        <div class="card-header">
            Chat box - {{ $seller->name }}
        </div>
        <div class="card-body">
            @if (count($messages) > 0)
                <form method="POST" action="{{ route('chats.store') }}">
                    @csrf
                    <label for="message">Type here:</label>
                    <input type="hidden" name="user" id="user" value="{{ $seller->name }}">
                    <div class="row">
                        <div class="col-sm-8">
                            <textarea class="form-control" style="width: 21rem" id="message" name="message"
                                placeholder="Type your message here"></textarea>
                        </div>
                        <input type="submit" value="Send" class="btn btn-success form-group"
                            onclick="this.disabled=true;this.value='Sending...';this.form.submit();"> &nbsp;
                        <a class="btn btn-primary form-group" href="{{ route('chats.index') }}">Back</a>
                    </div>
                </form> <br>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">From</th>
                            <th scope="col">Message</th>
                            <th scope="col">Date/Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($messages as $message)
                            <tr>
                                <td>{{ $message->user->name }}:</td>
                                <td>{{ $message->message }}</td>
                                <td>{{ $message->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Chat is Empty</p>
            @endif
        </div>
    </div>
@endsection
