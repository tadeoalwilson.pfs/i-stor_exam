@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('chats.store') }}">
        <div class="card mx-auto" style="width: 30rem">
            <div class="card-header">
                Send Message
            </div>
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="user">To:</label>
                    <input type="text" class="form-control" id="user" name="user" placeholder="Seller Name">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" id="message" name="message"
                        placeholder="Type your message here"></textarea>
                </div>
                <a class="btn btn-primary" href="{{ route('products.index') }}">Back</a>
                <input type="submit" value="Send" class="btn btn-success"
                    onclick="this.disabled=true;this.value='Sending...';this.form.submit();">
            </div>

        </div>
    </form>
@endsection
