@extends('layouts.app')

@section('content')
    <div class="card mx-auto" style="width: 35rem">
        <div class="card-header">
            Chat Inbox
        </div>
        <div class="card-body">
            <a href="{{ route('chats.create') }}" class="btn btn-primary">Send Message</a> <br><br>
            @if (count($contacts) > 0)
                <table class="table">
                    <tbody>
                        @foreach ($contacts as $contact)
                            <tr>
                                <td><a href="{{ route('chats.message', $contact->id) }}" name="to_user_id"
                                        id="to_user_id">{{ $contact->name }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Chat is Empty</p>
            @endif
            <div>{{ $contacts->links() }}</div>
        </div>
    </div>
@endsection
