-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2020 at 09:08 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `istor_exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Asus', '2020-08-27 17:16:48', '2020-08-27 17:16:48'),
(2, 1, 'Acer', '2020-08-27 17:16:55', '2020-08-27 17:16:55'),
(3, 2, 'Intel', '2020-08-27 17:24:45', '2020-08-27 17:24:45'),
(4, 2, 'AMD', '2020-08-27 17:24:50', '2020-08-27 17:24:50'),
(5, 2, 'Titan', '2020-08-27 17:24:58', '2020-08-27 17:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mouse', '2020-08-27 17:16:26', '2020-08-27 17:16:26'),
(2, 1, 'Monitor', '2020-08-27 17:16:36', '2020-08-27 17:16:36'),
(3, 2, 'KeyBoardo', '2020-08-27 17:22:11', '2020-08-28 00:40:19'),
(4, 2, 'CPU', '2020-08-27 17:22:37', '2020-08-28 00:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `user_id`, `to_user_id`, `message`, `created_at`, `updated_at`) VALUES
(39, 3, 1, 'A', '2020-08-28 18:23:33', '2020-08-28 18:23:33'),
(40, 1, 3, 'A', '2020-08-28 18:24:03', '2020-08-28 18:24:03'),
(41, 3, 1, 'A', '2020-08-28 18:24:41', '2020-08-28 18:24:41'),
(42, 4, 1, 'B', '2020-08-28 18:25:14', '2020-08-28 18:25:14'),
(43, 1, 4, 'B', '2020-08-28 18:25:33', '2020-08-28 18:25:33'),
(44, 3, 2, 'C', '2020-08-28 18:26:10', '2020-08-28 18:26:10'),
(45, 2, 3, 'C', '2020-08-28 18:26:34', '2020-08-28 18:26:34'),
(46, 4, 2, 'D', '2020-08-28 18:27:44', '2020-08-28 18:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(91, '2014_10_12_000000_create_users_table', 1),
(92, '2014_10_12_100000_create_password_resets_table', 1),
(93, '2019_08_19_000000_create_failed_jobs_table', 1),
(94, '2020_08_23_092926_create_products_table', 1),
(95, '2020_08_24_023725_create_brands_table', 1),
(96, '2020_08_24_023901_create_categories_table', 1),
(99, '2020_08_28_012931_create_orders_table', 2),
(101, '2020_08_28_085418_create_chats_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_id`, `count`, `total_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 1, 200, 'Received', '2020-08-27 23:28:59', '2020-08-28 00:38:39'),
(2, 4, 3, 1, 250, 'Received', '2020-08-27 23:29:06', '2020-08-28 00:38:50'),
(3, 4, 6, 1, 450, 'Received', '2020-08-27 23:29:50', '2020-08-28 00:39:39'),
(4, 3, 7, 1, 750, 'Received', '2020-08-27 23:30:13', '2020-08-28 00:39:25'),
(5, 3, 2, 1, 200, 'Received', '2020-08-27 23:30:54', '2020-08-28 00:38:59'),
(6, 4, 3, 5, 1250, 'Received', '2020-08-28 18:32:44', '2020-08-28 18:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `description`, `category_id`, `brand_id`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gaming mouse', 'gaming mouse', 1, 1, 100, 0, '2020-08-27 17:17:26', '2020-08-27 22:02:19'),
(2, 1, 'Gaming Monitor', 'Gaming monitor', 2, 2, 200, 28, '2020-08-27 17:17:56', '2020-08-27 23:30:54'),
(3, 1, 'ROG mouse', 'ROG mouse', 1, 1, 250, 4, '2020-08-27 17:18:21', '2020-08-28 18:32:44'),
(4, 1, '4k Monitor', 'Hi-Res monitor', 2, 1, 500, 40, '2020-08-27 17:18:59', '2020-08-27 17:18:59'),
(5, 2, 'intel core i5 7th gen', 'new intel processor', 4, 3, 600, 20, '2020-08-27 17:25:44', '2020-08-27 17:25:44'),
(6, 2, 'intel core i3 6th gen', 'intel core 13 processor', 4, 3, 450, 29, '2020-08-27 17:26:31', '2020-08-27 23:29:50'),
(7, 2, 'AMD Ryzen 3 320g', 'A gaming processor with built in GPU', 4, 4, 750, 59, '2020-08-27 17:27:08', '2020-08-27 23:30:13'),
(8, 2, 'Mechanical keyboard', 'New Gaming Keyboard', 3, 5, 250, 12, '2020-08-27 17:27:43', '2020-08-27 17:27:43'),
(9, 2, 'RGB keyboard', 'New Mechanical Gaming RGB Keyboard\r\nusb2.0', 3, 5, 599, 20, '2020-08-27 17:32:20', '2020-08-27 17:32:20'),
(10, 1, 'Curve Monitor', 'New Curve Gaming Monitor', 2, 1, 799, 6, '2020-08-27 17:33:24', '2020-08-27 17:33:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'seller1', 'seller1@seller.com', NULL, '$2y$10$eSW.1/U20W7k2Qz7EsOsbeAcAlbF7HnIBkEw3ZX.X4zgAu33ZHyIK', 'seller', NULL, '2020-08-27 17:14:15', '2020-08-27 17:14:15'),
(2, 'seller2', 'seller2@seller.com', NULL, '$2y$10$raEE.rEr4vCxcJ8r1uq4aeur9dizB5iYRqLzBkB1NcZ9oBvjeKzSq', 'seller', NULL, '2020-08-27 17:14:41', '2020-08-27 17:14:41'),
(3, 'user1', 'user1@user.com', NULL, '$2y$10$mWFTti/EYg3/jQWnp/3nA.4h7./QtUUeUjmBgA3UZaDTggYubc0na', 'user', NULL, '2020-08-27 17:15:20', '2020-08-27 17:15:20'),
(4, 'user2', 'user2@user.com', NULL, '$2y$10$F1qUIMD0QTNr5TbocyJpAurHgxDWgTvr96MNgMN2E65ag8tCuf0WG', 'user', NULL, '2020-08-27 23:26:00', '2020-08-27 23:26:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
